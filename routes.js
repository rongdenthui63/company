let jwt = require('jsonwebtoken');
let utils = require('./utils')
const PRIVATE_KEY = 'MIICXgIBAAKBgQDHikastc8+I81zCg'
const routes = function (app, connectionPool) {
  app.get('/', function (req, res) {
    res.render('register')
  })
  app.get('/login', function (req, res) {
    res.render('login')
  })

  app.get('/home', function (req, res) {
    res.render('home')
  })
  app.get('/members', function (req, res) {
    res.render('members')
  })

  app.get('/product', function (req, res) {
    res.render('products')
  })

  app.get('/projects', function (req, res) {
    res.render('projects')
  })

  app.get('/projects/:id/tasks', function (req, res) {
    res.render('tasks')
  })


  app.get('/get-tasks', function (req, res) {
    console.log('req.query: ', req.query)
    let projectId = req.query.projectId
    let token = req.query.token
    try {
      member = jwt.verify(token, PRIVATE_KEY)
    } catch (error) {
      res.status(422).send("Not Authenticated.")
    }

    connectionPool.getConnection(function (err, connection) {
      if (err) console.log("Can not connect to Database: ", err)
      connection.query(
        `SELECT * FROM projects_has_members WHERE projects_id=${projectId} AND members_id=${member.id}`,
        function (err, result, fields) {
          let project = result[0]
          if (project) {
            // console.log('----->', `SELECT * FROM tasks WHERE projects_id=${projectId}`)
            connection.query(
              `SELECT * FROM tasks WHERE projects_id=${projectId}`, function (err, tasks) {
                console.log('tasks: ', tasks)
                res.send(tasks)
              })
          } else {
            res.send('Not Authorized.')
          }
        }
      )
      connection.release()
    })
  })

  app.post('/register-submit', function (req, res) {
    connectionPool.getConnection(function (err, connection) {

      const body = req.body
      const name = body.name
      const email = body.email
      const password = body.password
      if (err) console.log("Can not connect to Database: ", err)
      connection.query(
        `INSERT INTO members (name, email, password, created_at) VALUES ("${name}", "${email}","${password}", "${utils.convertDateTimeForSQL()}");`,
        function (err, result, fields) {
          if (err) {
            if (err.code == 'ER_DUP_ENTRY') {
              res.status(422).send({ err: 'duplicate email' })
              return
            }
          }

          res.send('ok')
        }
      )
      connection.release()
    })
  })

  app.post('/create-project', function (req, res) {

    let token = req.body.token
    let member
    try {
      member = jwt.verify(token, PRIVATE_KEY)
    } catch (error) {
      console.log("ERROR: ", error)
    }
    connectionPool.getConnection(function (connectError, connection) {
      const name = req.body.name
      if (connectError) {
        console.log("Can not connect to Database: ", connectError)
        res.status(422).send('Cannot connect to SQL')
      }
      else {
        connection.query(
          `SELECT * FROM projects WHERE members_id=${member.id} AND name="${name}";`,
          function (databaseError, result, fields) {
            if (result.length == 0) {
              connection.query(
                `INSERT INTO projects (name, members_id, created_at) VALUES ("${name}", "${member.id}", "${utils.convertDateTimeForSQL()}" );`,
                function (err, result, fields) {
                  if (err) throw err
                  console.log("result: ", result)
                  connection.query(
                    `INSERT INTO projects_has_members (members_id, projects_id) VALUES ("${member.id}", "${result.insertId}");`,
                    function (err, result2) {
                      res.send(result)
                    })
                }
              )


            }
            else {
              res.status(422).send(`project name ${name} existed.`)
            }
          }
        )

      }
      connection.release()
    })
  })

  app.get('/get-projects', function (req, res) {
    try {
      let payload = jwt.verify(req.query.token, PRIVATE_KEY)
      connectionPool.getConnection(function (err, connection) {
        if (err) {
          console.log("Can not connect to Database: ", err)
          res.status(422).send('Cannot connect to SQL')
        }
        else {
          connection.query(
            `SELECT * FROM projects WHERE members_id=${payload.id};`,
            function (err, result, fields) {
              if (err) throw err
              res.send(result)
            }
          )
        }
        connection.release()
      })
    } catch {
      res.status(401).send('Not Authorized.')
    }
  })



  app.post('/authenticate', function (req, res) {
    console.log()
    connectionPool.getConnection(function (err, connection) {
      const email = req.body.email
      const password = req.body.password

      console.log(`SELECT * FROM members WHERE email="${email}" AND password="${password}"`)
      connection.query(`SELECT * FROM members WHERE email="${email}" AND password="${password}"`,
        function (err, result) {
          console.log('result: ', result)
          let user = result[0]
          if (user) {
            let token = jwt.sign({ id: user.id }, PRIVATE_KEY)
            res.send({ token: token, name: user.name })
          }
          else {
            res.status(422).send({ err: "Email or Password not correct" })
          }
        })
      connection.release()
    })
  })


  app.get('/users/:id_user', function (req, res) {
    connectionPool.getConnection(function (err, connection) {
      connection.query(`SELECT * FROM users WHERE id=${req.params.id_user}`, function (err, result) {
        if (err) throw err
        console.log(result[0])
        res.render('user', { email: result[0] ? result[0].email : 'Khong co' })
      })
      connection.release()
    })
  })
  app.post('/create-task', function (req, res) {
    console.log('---->/create-task Vao')
    let token = req.body.memberId
    let member
    try {
      member = jwt.verify(token, PRIVATE_KEY)
    } catch (error) {
      console.log("ERROR: ", error)
    }
    console.log(member)

    connectionPool.getConnection(function (connectError, connection) {
      const content = req.body.content
      const process = req.body.process
      const projectId = req.body.projectId
      console.log(projectId)
      connection.query(
        `INSERT INTO tasks (content, process, projects_id, projects_members_id, created_at) VALUES ("${content}","${process}","${projectId}", "${member.id}", "${utils.convertDateTimeForSQL()}" );`,
        function (err, result, fields) {
          if (err) throw err
          console.log("result: ", result)
          connection.query(
            `INSERT INTO tasks_has_members (members_id, tasks_id) VALUES ("${member.id}", "${result.insertId}");`,
            function (err, result2) {
              res.send(result)
            })
        }
      )

    })
  })
  app.post('/move-open', function (req, res) {

    connectionPool.getConnection(function (connectError, connection) {

      const taskId = req.body.taskId

      console.log(`update tasks
      set process='doing'
      where id='${taskId}';`)

      connection.query(
        `update tasks
        set process='doing'
        where id='${taskId}';`,

      )
      res.send({ status: 'ok' })


    })

  })
  app.post('/move-doing', function (req, res) {

    connectionPool.getConnection(function (connectError, connection) {

      const taskId = req.body.taskId


      connection.query(
        `update tasks
        set process='finish'
        where id='${taskId}';`,

      )
      res.send({ status: 'ok' })


    })

  })
  app.post('/move-finish', function (req, res) {

    connectionPool.getConnection(function (connectError, connection) {

      const taskId = req.body.taskId



      connection.query(
        `update tasks
        set process='doing'
        where id='${taskId}';`,

      )
      res.send({ status: 'ok' })


    })

  })


}


module.exports = routes
