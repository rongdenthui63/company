-- MySQL dump 10.13  Distrib 8.0.16, for osx10.14 (x86_64)
--
-- Host: localhost    Database: company
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `members`
--

LOCK TABLES `members` WRITE;
/*!40000 ALTER TABLE `members` DISABLE KEYS */;
INSERT INTO `members` VALUES (1,'long','rongdenthui63@gmail.com','rongden','2019-07-06 06:25:46',NULL),(2,'','rongdenthui1@gmail.com','rongden','2019-07-07 08:12:43',NULL),(3,'','rongdenthui2@gmail.com','rongden','2019-07-07 08:13:02',NULL),(4,'','rongdenthui3@gmail.com','connectionPool.getConnection(function (connectError, connection) {        const taskId = req.body.taskId          connection.query(         `update tasks         set process=\'doing\'         where id=\'${taskId}\';`,        )       res.send({ status: \'ok\' })','2019-07-07 08:13:16',NULL);
/*!40000 ALTER TABLE `members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `members_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_projects_members1_idx` (`members_id`),
  CONSTRAINT `fk_projects_members1` FOREIGN KEY (`members_id`) REFERENCES `members` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` VALUES (1,'mma','2019-07-06 06:26:03',NULL,1),(2,'klmlmlkmkl','2019-07-07 08:42:30',NULL,1);
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_has_members`
--

DROP TABLE IF EXISTS `projects_has_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `projects_has_members` (
  `projects_id` int(11) NOT NULL,
  `members_id` int(11) NOT NULL,
  PRIMARY KEY (`projects_id`,`members_id`),
  KEY `fk_projects_has_members_members1_idx` (`members_id`),
  KEY `fk_projects_has_members_projects_idx` (`projects_id`),
  CONSTRAINT `fk_projects_has_members_members1` FOREIGN KEY (`members_id`) REFERENCES `members` (`id`),
  CONSTRAINT `fk_projects_has_members_projects` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_has_members`
--

LOCK TABLES `projects_has_members` WRITE;
/*!40000 ALTER TABLE `projects_has_members` DISABLE KEYS */;
INSERT INTO `projects_has_members` VALUES (1,1),(2,1);
/*!40000 ALTER TABLE `projects_has_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `process` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `projects_id` int(11) NOT NULL,
  `projects_members_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`projects_id`,`projects_members_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` VALUES (1,'dam boc','doing','2019-07-06 06:26:27',NULL,1,1),(2,'123','doing','2019-07-06 06:26:55',NULL,1,1),(3,'kkkkkk','doing','2019-07-06 11:12:12',NULL,1,1),(4,'AAAAAAAAA','doing','2019-07-06 11:13:21',NULL,1,1),(5,'asdasdasdasdasdas1112','doing','2019-07-06 11:49:48',NULL,1,1),(6,'doing','doing','2019-07-06 11:49:48',NULL,1,1),(7,'text','doing','2019-07-07 04:45:54',NULL,1,1),(8,',ml','doing','2019-07-07 06:29:44',NULL,1,1),(9,'aaaaaa','doing','2019-07-07 06:34:59',NULL,1,1);
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks_has_members`
--

DROP TABLE IF EXISTS `tasks_has_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tasks_has_members` (
  `tasks_id` int(11) NOT NULL,
  `members_id` int(11) NOT NULL,
  PRIMARY KEY (`tasks_id`,`members_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks_has_members`
--

LOCK TABLES `tasks_has_members` WRITE;
/*!40000 ALTER TABLE `tasks_has_members` DISABLE KEYS */;
INSERT INTO `tasks_has_members` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(7,1),(8,1),(9,1);
/*!40000 ALTER TABLE `tasks_has_members` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-07 16:30:22
