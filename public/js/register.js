function checkemail() {
    let email = document.getElementById("email-input").value

    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (email == "") {
        alert("Email không thể bỏ trống");
        // email.focus();
        return false;
    }
    else {

        return re.test(String(email))
    }
}


function checkpassword() {
    let password = document.getElementById("password-input").value
    let re = /(?=.*\d)(?=.* [a - z])(?=.* [A - Z]).{ 6,}/;
    if (password == "") {
        alert("Password không thể bỏ trống");
        password.value.focus();
        return false;
    }
    else {
        return re.test(String(password).toLowerCase());
    }
};
function checkconfirmpassword() {
    let password = document.getElementById("password-input").value

    let confirmpassword = document.getElementById("confirm-password").value
    if (confirmpassword == "") {
        alert("Confirm Password không thể bỏ trống");
        // confirmpassword.focus();
        return false;
    }
    if (confirmpassword == password) {
        return true;
    }
    else {
        alert('Nhap lai password')

        return false;
    }
};

function register(event) {
    event.preventDefault()
    let textDom = document.getElementById('error_text')
    textDom.textContent = ''
    textDom.style.display = 'none'
    if (checkemail()) {
        if (checkconfirmpassword()) {
            let name = document.getElementById("name-input").value
            let email = document.getElementById("email-input").value
            let password = document.getElementById("password-input").value

            axios.post('/register-submit', {
                name: name,
                email: email,
                password: password,

            }).then(rs => {
                location.href = "/login"
            }).catch(err => {
                textDom.textContent = err.response.data.err

                textDom.style.display = 'block'
            })
        }

    }
    else {
        alert("Email khong hop le");
        return false;
    }


}