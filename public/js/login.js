function login(event) {
    event.preventDefault();
    let loginErr = document.getElementById("error_login")
    loginErr.textContent = ""
    loginErr.style.display = 'none'

    let emailValue = document.getElementById("email_input").value
    let passwordValue = document.getElementById("password_input").value

    axios.post('/authenticate', {
        email: emailValue,
        password: passwordValue
    }).then(function (rs) {
        localStorage.setItem('token', rs.data.token)
        localStorage.setItem('user_name', rs.data.name)
        location.href = "/projects"
    }).catch(err => {

        loginErr.textContent = "Email or Password not correct"
        loginErr.style.display = 'block'

    })
}