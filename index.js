var express = require('express')
var exphbs = require('express-handlebars')
let mysql = require('mysql')
let routes = require('./routes')

const PORT = 4000

var app = express()
// Parse URL-encoded bodies (as sent by HTML forms)
app.use(express.urlencoded())

// Parse JSON bodies (as sent by API clients)
app.use(express.json())
app.use(express.static('public'))
app.engine('handlebars', exphbs())
app.set('view engine', 'handlebars')

let connectionPool = mysql.createPool({
  // connectionLimit: 5,
  host: 'localhost',
  user: 'root',
  password: 'password',
  database: 'company'
})

routes(app, connectionPool)

let successConnectFunc = function () {
  console.log(`Node server opening on port ${PORT}!`)
}

app.listen(PORT, successConnectFunc)

