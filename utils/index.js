// convert type Datetime
const convertDateTimeForSQL = (date = new Date()) => {
    return new Date(date).toISOString().slice(0, 19).replace('T', ' ');
}

const Utils = {
    convertDateTimeForSQL: convertDateTimeForSQL
}

module.exports = Utils